\contentsline {section}{\numberline {1}Wst\IeC {\k e}p}{6}{section.1}
\contentsline {subsection}{\numberline {1.1}Metody kompilacji i wykonywania programu}{6}{subsection.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}Kompilacja Ahead-Of-Time}{6}{subsubsection.1.1.1}
\contentsline {paragraph}{}{6}{section*.3}
\contentsline {subsubsection}{\numberline {1.1.2}Kompilacja Just-In-Time}{6}{subsubsection.1.1.2}
\contentsline {paragraph}{}{7}{section*.5}
\contentsline {subsection}{\numberline {1.2}Optymalizacja r\IeC {\k e}czna}{7}{subsection.1.2}
\contentsline {paragraph}{}{7}{section*.6}
\contentsline {paragraph}{}{7}{section*.7}
\contentsline {section}{\numberline {2}Instrukcje rozga\IeC {\l }\IeC {\k e}ziaj\IeC {\k a}ce}{8}{section.2}
\contentsline {subsection}{\numberline {2.1}Wst\IeC {\k e}p teoretyczny do zagadnienia instrukcji rozga\IeC {\l }\IeC {\k e}ziaj\IeC {\k a}cych}{8}{subsection.2.1}
\contentsline {paragraph}{}{8}{section*.8}
\contentsline {subsubsection}{\numberline {2.1.1}Przyk\IeC {\l }adowe instrukcje rozga\IeC {\l }\IeC {\k e}ziaj\IeC {\k a}ce}{8}{subsubsection.2.1.1}
\contentsline {paragraph}{}{8}{section*.9}
\contentsline {paragraph}{}{8}{section*.10}
\contentsline {subsection}{\numberline {2.2}Rodzina instrukcji CMOVcc}{9}{subsection.2.2}
\contentsline {paragraph}{}{9}{section*.12}
\contentsline {subsection}{\numberline {2.3}Instrukcje steruj\IeC {\k a}ce przep\IeC {\l }ywem, a przetwarzanie potokowe}{9}{subsection.2.3}
\contentsline {paragraph}{}{9}{section*.14}
\contentsline {subsubsection}{\numberline {2.3.1}Predykcja statyczna}{10}{subsubsection.2.3.1}
\contentsline {paragraph}{}{10}{section*.15}
\contentsline {paragraph}{}{10}{section*.17}
\contentsline {section}{\numberline {3}Dekompozycja instrukcji rozga\IeC {\l }\IeC {\k e}ziaj\IeC {\k a}cych}{11}{section.3}
\contentsline {subsection}{\numberline {3.1}Ograniczenia optymalizacyjne kompilator\IeC {\'o}w}{11}{subsection.3.1}
\contentsline {paragraph}{}{11}{section*.20}
\contentsline {subsection}{\numberline {3.2}Optymalizacja instrukcji steruj\IeC {\k a}cych przep\IeC {\l }ywem w j\IeC {\k e}zykach wysokiego poziomu}{11}{subsection.3.2}
\contentsline {paragraph}{}{12}{section*.22}
\contentsline {paragraph}{}{13}{section*.24}
\contentsline {subsubsection}{\numberline {3.2.1}Alternatywne przypadki testowe}{13}{subsubsection.3.2.1}
\contentsline {section}{\numberline {4}\IeC {\'S}rodowisko testowe}{14}{section.4}
\contentsline {subsection}{\numberline {4.1}Specyfikacja komputera u\IeC {\.z}ywanego w badaniach}{14}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Badane kompilatory oraz \IeC {\'s}rodowiska maszyn wirtualnych}{14}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}J\IeC {\k e}zyk C}{14}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}J\IeC {\k e}zyk C++}{14}{subsubsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.3}J\IeC {\k e}zyk C Sharp}{14}{subsubsection.4.2.3}
\contentsline {subsubsection}{\numberline {4.2.4}J\IeC {\k e}zyk Java}{14}{subsubsection.4.2.4}
\contentsline {section}{\numberline {5}Eksperymentalne testy wydajno\IeC {\'s}ci proponowanej dekompozycji}{15}{section.5}
\contentsline {subsection}{\numberline {5.1}Metodologia bada\IeC {\'n}}{15}{subsection.5.1}
\contentsline {paragraph}{}{15}{section*.28}
\contentsline {paragraph}{}{15}{section*.29}
\contentsline {subsection}{\numberline {5.2}Wyniki oraz wnioski dla kod\IeC {\'o}w kompilowanych AOT}{16}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}Testy wydajno\IeC {\'s}ci dla j\IeC {\k e}zyka C}{16}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}Analiza wynik\IeC {\'o}w bada\IeC {\'n} dla j\IeC {\k e}zyka C}{16}{subsubsection.5.2.2}
\contentsline {paragraph}{}{16}{section*.31}
\contentsline {paragraph}{}{16}{section*.32}
\contentsline {subsubsection}{\numberline {5.2.3}Testy wydajno\IeC {\'s}ci dla j\IeC {\k e}zyka C++}{17}{subsubsection.5.2.3}
\contentsline {subsubsection}{\numberline {5.2.4}Analiza wynik\IeC {\'o}w bada\IeC {\'n} dla j\IeC {\k e}zyka C++}{17}{subsubsection.5.2.4}
\contentsline {paragraph}{}{17}{section*.34}
\contentsline {subsubsection}{\numberline {5.2.5}Rozbie\IeC {\.z}no\IeC {\'s}\IeC {\'c} mi\IeC {\k e}dzy badanymi kompilatorami}{17}{subsubsection.5.2.5}
\contentsline {paragraph}{}{17}{section*.35}
\contentsline {paragraph}{}{18}{section*.36}
\contentsline {subsection}{\numberline {5.3}Wyniki oraz wnioski dla wybranych \IeC {\'s}rodowisk maszyn wirtualnych}{18}{subsection.5.3}
\contentsline {subsubsection}{\numberline {5.3.1}Testy wydajno\IeC {\'s}ci dla j\IeC {\k e}zyka C Sharp}{18}{subsubsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.2}Problemy niestabilnego \IeC {\'s}rodowiska uruchomieniowego}{18}{subsubsection.5.3.2}
\contentsline {subsubsection}{\numberline {5.3.3}Analiza wynik\IeC {\'o}w bada\IeC {\'n} dla j\IeC {\k e}zyka C Sharp}{18}{subsubsection.5.3.3}
\contentsline {subsubsection}{\numberline {5.3.4}Testy wydajno\IeC {\'s}ci dla j\IeC {\k e}zyka Java}{19}{subsubsection.5.3.4}
\contentsline {subsubsection}{\numberline {5.3.5}Analiza wynik\IeC {\'o}w bada\IeC {\'n} dla j\IeC {\k e}zyka Java}{19}{subsubsection.5.3.5}
\contentsline {paragraph}{}{19}{section*.39}
\contentsline {section}{\numberline {6}Podsumowanie}{20}{section.6}
\contentsline {paragraph}{}{20}{section*.40}
\contentsline {paragraph}{}{20}{section*.41}
\contentsline {section}{\numberline {7}Bibliografia}{21}{section.7}
\contentsline {section}{\numberline {8}Za\IeC {\l }\IeC {\k a}czniki}{23}{section.8}
\contentsline {subsection}{\numberline {8.1}Testowany kod \IeC {\'z}r\IeC {\'o}d\IeC {\l }owy dla j\IeC {\k e}zyka C++}{23}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Testowany kod \IeC {\'z}r\IeC {\'o}d\IeC {\l }owy dla j\IeC {\k e}zyka C Sharp}{24}{subsection.8.2}
\contentsline {subsection}{\numberline {8.3}Testowany kod \IeC {\'z}r\IeC {\'o}d\IeC {\l }owy dla j\IeC {\k e}zyka Java}{25}{subsection.8.3}
\contentsline {section}{\numberline {9}Spis odwo\IeC {\l }a\IeC {\'n}}{26}{section.9}
