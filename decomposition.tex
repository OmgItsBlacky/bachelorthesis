\section{Dekompozycja instrukcji rozgałęziających}
Większość współczesnych popularnych środowisk wspierających produkcje oprogramowania posiada zestaw odpowiednich \textit{przełączników} decydujących o tym, czy ma zachodzić, i jeśli tak, to jak silna optymalizacja. W przypadku badanych kompilatorów były to flagi optymalizacyjne np. \textit{-O1} czy \textit{-O2}, natomiast dla środowisk maszyn wirtualnych były to odpowiednie ustawienia projektu np. dla środowiska \textit{.NET Core 2} flaga \textit{-c Release}.

\subsection{Ograniczenia optymalizacyjne kompilatorów}
Wspomniane mechanizmy optymalizacyjne kompilatorów w większości naiwnych przypadków bardzo dobrze sobie radzą z doborem  odpowiednich instrukcji rozgałęziających dla warunków logicznych kodu wysokiego poziomu. Biorąc za przykład kod z listingu (Lis. \ref{fig:simpleForLoop}), który został skompilowany z flagą \textit{-O2} można zauważyć, że kompilator \textit{GCC} w wersji \textit{8.2.1} wybrał specjalnie stworzoną do takich zadań instrukcję --- \textbf{CMOVE}.

\vspace{1em}
\begin{figure}[H]
\begin{minipage}{.47\textwidth}
\centering	
\begin{minted}[frame=single,linenos,baselinestretch=0.9]{cpp}
int i;
int res = 0;

for (i = 0; i < range; ++i) {
   if (VALUE == data[i]) {
       res = i;
   }
}   
\end{minted}
\captionof{listing} {Funkcja znajdująca ostatni indeks elementu w tablicy o zdanej wartości}
\label{fig:simpleForLoop}
\end{minipage}%
\hfill
\begin{minipage}{.45\textwidth}
\centering
\begin{minted}[frame=single,linenos,baselinestretch=0.75]{gas}
nopl   rax
mov    rcx,rdx
cmpb   rdi,rdx,1
ea     rdx,rcx

cmove  edx,eax  

cmp    rdx,rsi
\end{minted}
\captionof{listing} {Reprezentatywny fragment kodu maszynowego pętli \textit{for} z \textbf{Listingu \ref{fig:simpleForLoop}}}
\label{fig:simpleForLoopAsm}
\end{minipage}
\end{figure}

\paragraph{}
Problem stanowią zadania bardziej złożone. Jeśli nie są rozważane operacje wykonywane tylko~w~oparciu  o zmianę wartości na jednym rejestrze, który dodatkowo używany jest w czasie iteracji pętli, kompilator nie ma do dyspozycji odpowiedniego zestawu flag --- ustawionych wartości w rejestrach. W takiej sytuacji w praktyce nie jest możliwe wytypowanie odpowiednich dla danej akcji instrukcji predykcji odgałęzienia.

\subsection{Optymalizacja instrukcji sterujących przepływem w językach wysokiego poziomu}
Do prezentacji proponowanego zabiegu optymalizacyjnego został wykorzystany problem sortowania tablicy przy użyciu standardowego algorytmu sortowania bąbelkowego. 
Z uwagi na charakterystykę działania algorytmu odpowiednio przygotowane dane wejściowe pozwalają z niezwykłą łatwością kontrolować liczbę wykonywanych przeniesień warunkowych. Kod funkcji sortującej w wersji podstawowej, który później w analizie wyników badań  wykorzystywany jest jako referencyjna grupa kontrolna, został przedstawiony w listingu (Lis. \ref{fig:bubbleStd}).
W ramach analizy zachowań badanych kompilatorów w obliczu prezentowanej modyfikacji kodu źródłowego przedstawiono również przykładowy zdekompilowany kod wewnętrznej pętli \textit{for} funkcji sortującej dla przypadku standardowego --- \textit{\textbf{Wariant A}} oraz optymalizowanego --- \textit{\textbf{Wariant B}}. 
Wspomniane kody języka maszynowego w celu ułatwienia analizy zostały oczyszczone --- usunięto dodatkowe wygenerowane etykiety niezwiązane z analizowanym problemem oraz uproszczony został zapis wyliczanych przesunięć w tablicy sortowanych danych. W ten sposób uproszczone kody języka maszynowego zostały odpowiednio zamieszczone na listingach (Lis. \ref{fig:bubbleStdAsm}) oraz (Lis. \ref{fig:bubbleOptAsm}).

\begin{figure}[H]
\begin{minipage}{.46\textwidth}
\centering	
\label{listing}{\textbf{Wariant A}}
\vspace{0.5em}
\begin{minted}[frame=single,linenos,baselinestretch=0.9]{cpp}
int i; int j; 
int tmp;
 
for (i = 0; i < range; ++i) {
 for (j = 0; j < range - i; ++j) {
    if(data[j] > data[j+1]) {
      tmp = data[j];
      data[j] = data[j+1];
      data[j+1] = tmp;
    }
  }
}
\end{minted}
\captionof{listing} {Podstawowa funkcja sortująca przed optymalizacją w wariancie dla języka \textit{C} oraz \textit{C++}}
\label{fig:bubbleStd}
\end{minipage}%
\hfill
\begin{minipage}{.46\textwidth}
\centering	
\begin{minted}[frame=single,linenos,baselinestretch=0.75]{gas}
.LBB0_2:
  mov     edx, dword ptr [rdi]
  cmp     r8d, 1
  jne     .LBB0_10
  xor     ecx, ecx
  test    r8b, 1
  jne     .LBB0_6
  jmp     .LBB0_8
  
.LBB0_11:
  mov     ecx, dword ptr [...]
  cmp     edx, ecx
  jle     .LBB0_12
  mov     dword ptr [...], ecx
  mov     dword ptr [...], edx
  jmp     .LBB0_14
\end{minted}
\captionof{listing} {Oczyszczony przykładowy kod maszynowy dla wewnętrznej pętli \textit{for} w kodzie z (Lis. \ref{fig:bubbleStd}) wygenerowany przez kompilator clang 7.0.0 przy kompilacji z flagą \textbf{\textit{-O2}}}
\label{fig:bubbleStdAsm}
\end{minipage}
\end{figure}

\paragraph{}
W wielu doświadczeniach przeprowadzonych w ramach niniejszej pracy nie udało się zmusić kompilatora do wygenerowania instrukcji przeniesienia warunkowego --- sprawdzane były różne kompilatory w różnych wersjach oraz różne kombinacje flag kompilacji. W standardowej wersji algorytmu żaden z wykorzystanych kompilatorów na chwilę obecną nie jest wstanie rozwinąć kodu wysokopoziomowego do odpowiednich instrukcji kodu maszynowego. Niezależnie od zastosowanego mechanizmu sterującego przepływem, tj. użycia operatora \textit{trójargumentowego}, instrukcji \textit{switch}, pętli z odpowiednim warunkiem, czy instrukcji \textit{if} w dowolnej kombinacji, nie skutkowało to wygenerowaniem odpowiednich instrukcji kodu maszynowego.
W obliczu przedstawionego problemu została zaproponowana metoda ręcznej optymalizacji, przedstawiona na (Lis. \ref{fig:bubbleOpt}).

\begin{figure}[H]
\begin{minipage}{.46\textwidth}
\centering
\label{listing}{\textbf{Wariant B}}
\vspace{0.5em}
\begin{minted}[frame=single,linenos,baselinestretch=0.9]{cpp}
int i; int j; 
int tmp_A; int tmp_B;
 
for (i = 0; i < range; ++i) {
  for (j = 0; j < range - i; ++j) {
    tmp_A = data[j];
    tmp_B = data[j+1];
 
    data[j] = tmp_B < tmp_A ? 
      tmp_B : tmp_A;
    
    data[j+1] = tmp_B < tmp_A ? 
      tmp_A : tmp_B;
  }
}
\end{minted}
\captionof{listing} {Proponowana optymalizacja funkcji sortującej w wariancie dla języka \textit{C} oraz \textit{C++}}
\label{fig:bubbleOpt}
\end{minipage}%
\hfill
\begin{minipage}{.46\textwidth}
\centering
\begin{minted}[frame=single,linenos,baselinestretch=0.75]{gas}
.LBB0_2:
  mov     ebx, dword ptr [rdi]
  cmp     r8d, 1
  jne     .LBB0_5

.LBB0_6:
  mov     ebx, dword ptr [...]
  mov     edx, dword ptr [...]
  cmp     ebx, eax
  mov     ebp, eax
  cmovle  ebp, ebx
  mov     dword ptr [...], ebp
  cmovl   ebx, eax
  cmp     edx, ebx
  mov     eax, ebx
  cmovle  eax, edx
  cmovge  ebx, edx
  mov     dword ptr [...], eax
  mov     dword ptr [...], ebx  
\end{minted}
\captionof{listing} {Oczyszczony przykładowy kod maszynowy dla wewnętrznej pętli \textit{for} w kodzie z (Lis. \ref{fig:bubbleOpt}) wygenerowany przez kompilator clang 7.0.0 przy kompilacji z flagą \textbf{\textit{-O2}}}
\label{fig:bubbleOptAsm}
\end{minipage}
\end{figure}

\paragraph{}
Wstępnie analizując listing kodu maszynowego (Lis. \ref{fig:bubbleOptAsm}) można zauważyć, iż zastosowanie dodatkowych zmiennych, czyli bezpośrednie użycie potrzebnych rejestrów, umożliwia późniejsze wykorzystanie ich przez kompilator. Pozwala to na wygenerowanie instrukcji z rodziny \textbf{CMOVcc} przy kompilacji kodu z flagami optymalizacyjnymi \textit{\textbf{-O1}} oraz \textbf{\textit{-O2}}. 

\subsubsection{Alternatywne przypadki testowe}
Istnieje słuszne pytanie, czy samo użycie operatora trójargumentowego przyspieszy wykonanie kodu i czy dodatkowe operacje przypisania na wygenerowanych zmiennych jedynie przeszkadzają. Zostały zatem zaproponowane dwa dodatkowe przypadki badanego kodu, mające na celu rozwianie wątpliwości związanych z proponowanym zabiegiem optymalizacyjnym. Alternatywne wersje zostały przedstawione na listingach (Lis. \ref{fig:bubbleAlterTest1}) oraz (Lis. \ref{fig:bubbleAlterTest2}) --- algorytm przedstawiony w tych przypadkach testowych został odpowiednio zmodyfikowany, każda iteracja zewnętrznej pętli \textit{for} zeruje zmienne tymczasowe. Tak zmodyfikowana funkcja sortująca może być wykorzystywana tylko do sortowania wartości dodatnich.

\vspace{1em}
\begin{figure}[H]
\begin{minipage}{.47\textwidth}
\centering
\label{listing}{\textbf{Wariant C}}
\vspace{0.5em}
\begin{minted}[frame=single,linenos,baselinestretch=0.9]{cpp}
int i; int j; 
int tmp;

for (i = 0; i < range; ++i) {
  tmp = 0;
  
  for (j = 0; j < range - i; ++j) {
    if(data[j] > data[j+1]) {
      tmp = data[j];
    }

    if(data[j] > data[j+1]) {
      data[j] = data[j+1];
    }
       
    if(tmp > data[j+1]) {
      data[j+1] = tmp;
    }  
  }
}
\end{minted}
\captionof{listing} {Alternatywna wersja doświadczalna z instrukcjami warunkowymi w postaci \textit{\textbf{if}}}
\label{fig:bubbleAlterTest1}
\end{minipage}%
\hfill
\begin{minipage}{.47\textwidth}
\centering
\label{listing}{\textbf{Wariant D}}
\vspace{0.5em}
\begin{minted}[frame=single,linenos,baselinestretch=0.9]{cpp}
int i; int j; 
int tmp;

for (i = 0; i < range; ++i) {
  tmp = 0;

  for (j = 0; j < range - i; ++j)  {
    tmp = data[j] > data[j+1] ? 
      data[j] : tmp;     
    
    data[j] = data[j] > data[j+1] ? 
      data[j+1] : data[j];   
    
    data[j+1] = tmp > data[j+1] ?  
      tmp : data[j+1];
  }
} 
\end{minted}
\captionof{listing} {Alternatywna wersja doświadczalna z instrukcjami warunkowymi w postaci operatora trójargumentowego}
\label{fig:bubbleAlterTest2}
\end{minipage}
\end{figure}
