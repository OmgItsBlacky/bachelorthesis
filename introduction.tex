\section{Wstęp}
W procesie wytwarzania systemów informatycznych zagadnienie wydajnego działania oprogramowania poruszane jest na każdym etapie produkcji. Diametralny wzrost mocy obliczeniowej sprzętu komputerowego wykonującego programy nie rekompensuje rosnącego poziomu wymagań współczesnych systemów informatycznych. Powszechnie wiadome jest, że nie da się w nieskończoność zmniejszać procesu technologicznego układów scalonych, co do tej pory stanowiło kluczowy czynnik w poprawie ich szybkości.
W obliczu ograniczeń dzisiejszych technologii właściwe zrozumienie architektury komputerów i różnorodnych technik optymalizacyjnych będzie miało kluczowe znaczenie w procesie wytwarzania nowoczesnych i wydajnych systemów informatycznych.

\subsection{Metody kompilacji i wykonywania programu}
Stosowane technologie oraz zestaw zasadniczych technik w procesie tworzenia oprogramowania niejednokrotnie warunkują liczbę kroków, w których może być przeprowadzona optymalizacja.

\subsubsection{Kompilacja Ahead-Of-Time}
Sposób kompilacji \textit{Ahead-Of-Time}, w dalszej części nazywany kompilacją \textit{AOT}, rozumiany jest jako proces kompilacji języków wysokiego poziomu do kodu maszynowego. W ten sposób wygenerowany kod jest wykonywany przez konkretną platformę, czyli określoną wersję maszyny wirtualnej czy procesor o wybranej architekturze. Kompilacja \textit{AOT} jest przykładem kompilacji statycznej, wykonywanej tylko jeden raz, dla danej konfiguracji sprzętowej i od razu dla całego programu \cite{aot}. Wykorzystywana jest zarówno w technologiach korzystających ze środowisk maszyn wirtualnych, jak i w przypadku języków takich jak \textit{C} czy \textit{C++}.

\begin{figure}[H]
  \centering
  \begin{minipage}{1\textwidth}
  \includegraphics[width=1\linewidth]{Resources/aot}
  \captionof{figure} {Przebieg kompilacji AOT}
  \label{fig:aot}
  \end{minipage}  
\end{figure}

\paragraph{}
Analizując przebieg kompilacji zgodnie z diagramem (Rys. \ref{fig:aot}), można zauważyć jedynie dwa etapy, w których możliwe jest przeprowadzenie optymalizacji --- optymalizacja ręczna albo optymalizacja wykonywana przez kompilator. Program skompilowany w ten sposób jest w pełni niezmienny i za każdym razem wykonywany zgodnie z wygenerowanym kodem maszynowym.

\subsubsection{Kompilacja Just-In-Time}
Kompilacja \textit{Just-In-Time} rozumiana jest jako metoda dynamicznego wykonania programów komputerowych, polegająca na ich kompilacji do kodu maszynowego bezpośrednio przed wykonaniem danego fragmentu kodu źródłowego. Jest ona w dalszej części pracy nazywana kompilacją \textit{JIT} albo \textit{run-time}. Obecnie jest ona najczęściej używaną metodą kompilacji dla środowisk maszyn wirtualnych \cite{jit}. 

\begin{figure}[H]
  \centering
  \begin{minipage}{1\textwidth}
  \includegraphics[width=1\linewidth]{Resources/jit}
  \captionof{figure} {Przebieg kompilacji i wykonania programu JIT}
  \label{fig:jit}
  \end{minipage}  
\end{figure}

\paragraph{}
Przedstawiony na diagramie (Rys. \ref{fig:jit}) przebieg kompilacji \textit{JIT} uwidacznia dodatkowy etap optymalizacji, dokonywany przez środowisko maszyny wirtualnej. Na tym etapie kod pośredni wygenerowany przez kompilator w czasie wykonania programu może być dodatkowo optymalizowany przez maszynę wirtualną. Efektem takiej optymalizacji mogą być inaczej generowane dla kolejnych uruchomień instrukcje kodu maszynowego.

\subsection{Optymalizacja ręczna}
Dla większości metodologii wytwarzania systemów informatycznych wspólnym etapem, na którym może być przeprowadzona optymalizacja, jest wstępne wytwarzanie oprogramowania --- tworzenie kodu źródłowego. W ramach tego kroku może zostać przeprowadzona ręczna \textit{optymalizacja lokalna}, której efekty będą propagowane przez wszystkie kolejne warstwy w procesie produkcyjnym \cite{code_manipulation}.

\paragraph{}
Ręczna modyfikacja kodu źródłowego w celach optymalizacyjnych może prowadzić do nieprzewidywalnych wyników. Biorąc pod uwagę poprawę jakości kompilatorów oraz prężnie rozwijające się  programy wspomagające statyczną analizę kodu, wielokrotnie już nieaktualne lub nieumiejętne zastosowane zabiegi optymalizacyjne nie tylko pogarszają czytelność kodu źródłowego, ale również utrudniają pracę wspomnianych narzędzi. Obecnie do najczęściej stosowanych technik optymalizacyjnych należą \cite{codemanipulation} :

\begin{itemize}
\item \textbf{optymalizacje dotycząca zmiennych oraz wyrażeń:}

\begin{itemize}
\setlength{\itemsep}{-0.2em}
\item zwijanie stałych (\textit{ang. constant folding}), 
\item propagacja kopii (\textit{ang. copy propagation}),
\item redukcja złożoności wyrażeń (\textit{ang. strength reduction}),
\item przemianowanie zmiennych (\textit{ang. variable renaming}),
\item eliminacja powtarzających się podwyrażeń (\textit{ang. common subexpression elimination}),
\end{itemize}
\item \textbf{optymalizacje wykonania pętli:}
\begin{itemize}
\setlength{\itemsep}{-0.2em}
\item uproszczenie wyrażeń zawierających indeks pętli (\textit{ang. induction variable simplification}),
\item usunięcie poza pętlę kodu niezależnego od iteracji (\textit{ang. loop invariant code motion}),
\item zmiana kolejności wykonania pętli (\textit{ang. loop interchange}),
\item łączenie pętli (\textit{ang. loop fusion}),
\item rozdzielanie pętli (\textit{ang. loop fission}),
\item rozwijanie pętli (\textit{ang. loop unrolling}),
\item grupowanie instrukcji ze względu na dostęp do pamięci podręcznej (\textit{ang. blocking}),
\end{itemize}

\item \textbf{optymalizacje na poziomie instrukcji:}
\begin{itemize}
\setlength{\itemsep}{-0.2em}
\item usuwanie nieosiągalnego lub produkującego zbędne dane kodu (\textit{ang. dead code removal}),
\item eliminacja rekurencji ogonowej (\textit{ang. tail-recursion elimination}),
\item wplatanie procedur - rozwijanie w miejscu wywołania (\textit{ang. inlining}),
\item pobieranie z wyprzedzeniem realizowane programowo (\textit{ang. software prefetching}),
\item przetwarzanie potokowe na poziomie kodu źródłowego (\textit{ang. software pipelining}). 
\end{itemize}
\end{itemize}

\paragraph{}
Większość z wymienionych technik optymalizacyjnych jest traktowana jako operacje doraźne. Prezentowane metody są niezależne od architektury sprzętowej czy wybranej technologii, jednak sensowność ich wykorzystania często uwarunkowana jest od określonego algorytmu. Systemy informatyczne z różnych dziedzin, używające różnych algorytmów, mogą korzystać z tych samych zabiegów optymalizacyjnych z ostatniej przytoczonej grupy, gdyż zdecydowana większość używa tych samych instrukcji niskopoziomowych. Wspomniane fakty bezpośrednio klasyfikują optymalizacje na poziomie instrukcji jako operacje o większym spektrum zastosowań.
