\contentsline {listing}{\numberline {1}{\ignorespaces Sekwencja przeniesienia warunkowego bez instrukcji CMOVcc\relax }}{9}{figure.caption.11}
\contentsline {listing}{\numberline {2}{\ignorespaces Sekwencja przeniesienia z u\IeC {\.z}yciem instrukcji CMOVcc\relax }}{9}{figure.caption.11}
\contentsline {listing}{\numberline {3}{\ignorespaces Funkcja znajduj\IeC {\k a}ca ostatni indeks elementu w tablicy o zdanej warto\IeC {\'s}ci\relax }}{11}{figure.caption.19}
\contentsline {listing}{\numberline {4}{\ignorespaces Reprezentatywny fragment kodu maszynowego p\IeC {\k e}tli \textit {for} z \textbf {Listingu \ref {fig:simpleForLoop}}\relax }}{11}{figure.caption.19}
\contentsline {listing}{\numberline {5}{\ignorespaces Podstawowa funkcja sortuj\IeC {\k a}ca przed optymalizacj\IeC {\k a} w wariancie dla j\IeC {\k e}zyka \textit {C} oraz \textit {C++}\relax }}{12}{figure.caption.21}
\contentsline {listing}{\numberline {6}{\ignorespaces Oczyszczony przyk\IeC {\l }adowy kod maszynowy dla wewn\IeC {\k e}trznej p\IeC {\k e}tli \textit {for} w kodzie z (Lis. \ref {fig:bubbleStd}) wygenerowany przez kompilator clang 7.0.0 przy kompilacji z flag\IeC {\k a} \textbf {\textit {-O2}}\relax }}{12}{figure.caption.21}
\contentsline {listing}{\numberline {7}{\ignorespaces Proponowana optymalizacja funkcji sortuj\IeC {\k a}cej w wariancie dla j\IeC {\k e}zyka \textit {C} oraz \textit {C++}\relax }}{12}{figure.caption.23}
\contentsline {listing}{\numberline {8}{\ignorespaces Oczyszczony przyk\IeC {\l }adowy kod maszynowy dla wewn\IeC {\k e}trznej p\IeC {\k e}tli \textit {for} w kodzie z (Lis. \ref {fig:bubbleOpt}) wygenerowany przez kompilator clang 7.0.0 przy kompilacji z flag\IeC {\k a} \textbf {\textit {-O2}}\relax }}{12}{figure.caption.23}
\contentsline {listing}{\numberline {9}{\ignorespaces Alternatywna wersja do\IeC {\'s}wiadczalna z instrukcjami warunkowymi w postaci \textit {\textbf {if}}\relax }}{13}{figure.caption.25}
\contentsline {listing}{\numberline {10}{\ignorespaces Alternatywna wersja do\IeC {\'s}wiadczalna z instrukcjami warunkowymi w postaci operatora tr\IeC {\'o}jargumentowego\relax }}{13}{figure.caption.25}
\contentsline {listing}{\numberline {11}{\ignorespaces Kod testowy dla j\IeC {\k e}zyka C++ wariant A\relax }}{23}{figure.caption.42}
\contentsline {listing}{\numberline {12}{\ignorespaces Kod testowy dla j\IeC {\k e}zyka C++ wariant B\relax }}{23}{figure.caption.42}
\contentsline {listing}{\numberline {13}{\ignorespaces Kod testowy dla j\IeC {\k e}zyka C++ wariant C\relax }}{23}{figure.caption.43}
\contentsline {listing}{\numberline {14}{\ignorespaces Kod testowy dla j\IeC {\k e}zyka C++ wariant D\relax }}{23}{figure.caption.43}
\contentsline {listing}{\numberline {15}{\ignorespaces Kod testowy dla j\IeC {\k e}zyka C Sharp wariant A\relax }}{24}{figure.caption.44}
\contentsline {listing}{\numberline {16}{\ignorespaces Kod testowy dla j\IeC {\k e}zyka C Sharp wariant B\relax }}{24}{figure.caption.44}
\contentsline {listing}{\numberline {17}{\ignorespaces Kod testowy dla j\IeC {\k e}zyka C Sharp wariant C\relax }}{24}{figure.caption.45}
\contentsline {listing}{\numberline {18}{\ignorespaces Kod testowy dla j\IeC {\k e}zyka C Sharp wariant D\relax }}{24}{figure.caption.45}
\contentsline {listing}{\numberline {19}{\ignorespaces Kod testowy dla j\IeC {\k e}zyka Java wariant A\relax }}{25}{figure.caption.46}
\contentsline {listing}{\numberline {20}{\ignorespaces Kod testowy dla j\IeC {\k e}zyka Java wariant B\relax }}{25}{figure.caption.46}
\contentsline {listing}{\numberline {21}{\ignorespaces Kod testowy dla j\IeC {\k e}zyka Java wariant C\relax }}{25}{figure.caption.47}
\contentsline {listing}{\numberline {22}{\ignorespaces Kod testowy dla j\IeC {\k e}zyka Java wariant D\relax }}{25}{figure.caption.47}
